FROM node as base

WORKDIR /app

COPY . /app

RUN npm install

#EXPOSE 5000

#CMD ["node" "server.js"]

###########################################
FROM node:10-alpine3.9

WORKDIR /app

COPY --from=base /app /app

EXPOSE 5000

CMD ["node" "server.js"]
####################################################



#FROM nginx:alpine

#COPY --from=base  /app/default.conf /etc/nginx/conf.d/

#COPY --from=base /app/index.html /usr/share/nginx/html/
#RUN chown nginx.nginx /usr/share/nginx/html/ -R

#EXPOSE 5000
#CMD ["nginx", "-g", "daemon off;"]
#CMD ["sleep", "10000"]
